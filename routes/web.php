<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);
Route::group(['middleware' => ['auth']], function () {
Route::post('email/resend', 'Auth\VerificationController@resend');
});
/////// Admin ///
Route::group(['middleware' => ['auth']], function () {
    Route::post('logout', 'Auth\AuthController@logout')->name('logout');
});

Route::get('login', 'Auth\AuthController@showLoginForm')->name('loginForm');
Route::post('login', 'Auth\AuthController@login')->name('login');
Route::group(['namespace' => 'Back', 'middleware' => ['verified', 'auth']], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/profile', 'UserController@getUpdateProfile')->name('get-update-profile');
    Route::patch('/profile', 'UserController@postUpdateProfile')->name('post-update-profile');
    //
    Route::group(['middleware' => ['permission:users|admin', 'MinifyHtml']], function () {
        Route::resource('users', 'UserController');
    });
    Route::group(['middleware' => ['permission:roles|admin', 'MinifyHtml']], function () {
        Route::resource('roles', 'RoleController');
    });
    Route::group(['middleware' => ['permission:permissions|admin', 'MinifyHtml']], function () {
        Route::resource('permissions', 'PermissionController');
    });
    Route::group(['middleware' => ['permission:sales|admin']], function () {
        Route::resource('sales', 'SalesController');

    });
    Route::group(['middleware' => ['permission:configrations|admin']], function () {
        Route::resource('configrations', 'ConfigrationsController');

    });

    Route::group(['middleware' => ['permission:settings|admin']], function () {
        Route::get('setting', 'SettingController@index')->name('setting.index');
        Route::post('setting', 'SettingController@update')->name('setting.update');
    });
});
