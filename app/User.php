<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    public $path_files = 'avatar/';
    protected $fillable = [
        'name', 'email', 'password','website'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function image()
    {
        return $this->morphOne('App\File', 'imageable');
    }

    public function uploadFile()
    {
        // upload file ar file
        $file = (!$this->image) ? new File() : File::find($this->image->id);
        if (request()->avatar) {
            $fileName = time() . '.' . request()->avatar->getClientOriginalExtension();
            $file->setTranslation('url', 'en', $fileName);
            $file->setTranslation('url', 'ar', $fileName);
            @unlink($this->path_files . @$this->image->translations['url']['ar']);
            request()->avatar->move($this->path_files, $fileName);
        }
        $file->type = 'img';
        (!$this->image) ? $file->save() : $file->update();
        return $file;
    }

    public function scopeNotSuperAdmin($q)
    {
        return $q->where('super_admin', '!=', 1);
    }
}
