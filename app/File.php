<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class File extends Model
{
    use HasTranslations;

    public $translatable = ['url'];
    protected $fillable = ['url','type'];
    protected $table = 'files';

    public function imageable()
    {
        return $this->morphTo();
    }
}
