<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class SettingController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']); //isAdmin middleware lets only users with a //specific permission permission to access these resources

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = \Setting::all(); //Get all permission
        return view('back.settings.index')->with('setting', $setting);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $array = $request->all();
        // remove token
        unset($array['_token']);
       // validation
        $this->validate($request, [
            'logo' => 'file|mimes:png|max:20000',
            'link' => 'url',
        ]);

        // settings

        if ($request->logo) {

            $fileName_ar = 'logo-white.' . request()->logo->getClientOriginalExtension();
            request()->logo->move(public_path('back/assets/global/images/logo/'), $fileName_ar);
            $array['logo'] = $fileName_ar;
        }
        // remove item null
        foreach ($array as $k => $ar) {
            if (!$ar)
                unset($array[$k]);
        }
        setting($array)->save();
        return redirect()->route('setting.index')
            ->with('flash_message',
                'Settings saved');
    }

}
