<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Salesman as Model;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Model::paginate();

        return view('back.sales.index')->with('sales', $sales);
    }
}
