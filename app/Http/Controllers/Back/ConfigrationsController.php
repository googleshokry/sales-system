<?php

namespace App\Http\Controllers\Back;


use App\Http\Controllers\Controller;

use App\Configration as Model;


class ConfigrationsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configrations = Model::paginate();

        return view('back.configrations.index')->with('configrations', $configrations);
    }


}
