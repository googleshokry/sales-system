<?php

namespace App\Http\Controllers\Back;

use App\AbstractSubmission;
use App\FullSubmission;
use App\Register;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use DB;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {

    }
    public function index()
    {
        $sales = DB::table('sales')->where('deleted_at',null)->count();
        $users = DB::table('users')->where('deleted_at',null)->count();
        return view('back.dashboard.index',compact('users','sales'));
    }

}
