<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
class AuthController extends Controller
{

    public function showLoginForm()
    {
        return view('back.auth.login');
    }
    public function login(Request $request)
    {
        $errors = new MessageBag; // initiate MessageBag

        //validate the fields....
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('login')
                ->withErrors($validator)
                ->withInput();
        }
        $credentials = ['email' => $request->email, 'password' => $request->password];
        if (Auth::attempt($credentials, $request->remember)) { // login attempt
            //login successful, redirect the user to your preferred url/route...
            return redirect()->route('dashboard');
        }
        $errors = new MessageBag(['password' => ['Email and/or password invalid.']]); // if Auth::attempt fails (wrong credentials) create a new message bag instance.

        return Redirect()->route('loginForm')->withErrors($errors)->withInput(Input::except('password')); // redirect back to the login page, using ->withErrors($errors) you send the error created above
//        return Redirect()->route('loginForm');
    }
    public function logout()
    {
        Auth::logout(); 
        return Redirect()->route('login');
    }
}
