<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Configration extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sale()
    {
        return $this->belongsTo(Salesman::class);
    }

}
