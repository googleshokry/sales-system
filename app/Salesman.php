<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Salesman extends Model
{
    protected $table = 'sales';

    public function config()
    {
        return $this->belongsTo(Configration::class);
    }

}
