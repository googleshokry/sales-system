Create database and set your connect in .env file

Commands
```
composer install
php artisan migrate
php artisan db:seed
php artisan key:generate
```

Super Admin
```
super Admin 
http://localhost:8000
username:super@admin.com
password:admin123
```
user Admin
```
http://localhost:8000
username:admin@admin.com
password:123456
```


please set mail config in env 
```
MAIL_DRIVER=smtp  
MAIL_HOST=smtp.mailtrap.io  
MAIL_PORT=2525  
MAIL_USERNAME=<YOUR_MAILTRAP_USERNAME>  
MAIL_PASSWORD=<YOUR_MAILTRAP_PASSWORD>  
MAIL_ENCRYPTION=tls
```