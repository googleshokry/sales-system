@extends('back.layout.master')

@section('title', '| Configrations')

@section('content')
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <h1><i class="fa fa-archive"></i> Configrations and settings</h1>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">

                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Target</th>
                        <th>Sale Name</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($configrations as $form)
                        <tr>
                            <td>{{ $form->id }}</td>
                            <td>{{ $form->target }}</td>
                            <td>{{ $form->sale->name }}</td>

                        </tr>
                    @endforeach
                    </tbody>

                </table>
                {{$configrations->links()}}

            </div>

        </div>
    </div>

@endsection
