@extends('back.layout.master')

@section('title', '| Sales')

@section('content')
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <h1><i class="fa fa-archive"></i> Sales</h1>
            <hr>

            <div class="table-responsive">
                <table class="table table-bordered table-striped">

                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Payment</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($sales as $form)
                        <tr>
                            <td>{{ $form->id }}</td>
                            <td>{{ $form->name }}</td>
                            <td>{{ $form->payment }}</td>

                        </tr>
                    @endforeach
                    </tbody>

                </table>
                {{$sales->links()}}

            </div>

        </div>
    </div>

@endsection
