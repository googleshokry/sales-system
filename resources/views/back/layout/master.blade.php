<!DOCTYPE html>
<html lang="en">
<head>
{{--    <base href="kitchen/public/">--}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="shortcut icon" href="{{asset('back/assets/global/images/logo/logo-white.png')}}" type="image/png">
    <title>Sales</title>
    <link href="{{asset('/back/assets/global/plugins/datatables/dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('back/assets/global/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('back/assets/global/css/theme.css')}}" rel="stylesheet">
    <link href="{{asset('back/assets/global/css/ui.css')}}" rel="stylesheet">
    <link href="{{asset('back/assets/admin/layout1/css/layout.css')}}" rel="stylesheet">
    <!-- BEGIN PAGE STYLE -->
    <link href="{{asset('back/assets/global/plugins/metrojs/metrojs.min.css')}}" rel="stylesheet">
    <link href="{{asset('back/assets/global/plugins/maps-amcharts/ammap/ammap.min.css')}}" rel="stylesheet">
    <!-- END PAGE STYLE -->
    <script src="{{asset('back/assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js')}}"></script>
    <!-- END PAGE STYLE -->
    <style>
        p.teaser {
            text-indent: 30px;
        }
        .text-right
        {
            text-align: right;
        }
        form.action,ul#tree1>li>form,form.inline {
            display: inline-block;
        }
        .boxclose{
            display:block;
            box-sizing:border-box;
            width:20px;
            height:20px;
            border-width:3px;
            border-style: solid;
            border-color:red;
            border-radius:100%;
            background: -webkit-linear-gradient(-45deg, transparent 0%, transparent 46%, white 46%,  white 56%,transparent 56%, transparent 100%), -webkit-linear-gradient(45deg, transparent 0%, transparent 46%, white 46%,  white 56%,transparent 56%, transparent 100%);
            background-color:red;
            box-shadow:0px 0px 5px 2px rgba(0,0,0,0.5);
            transition: all 0.3s ease;
            position: absolute;
            top: -10px;
            left: 143px;
        }

    </style>
</head>
@yield('css')
<body class="fixed-topbar fixed-sidebar theme-sdtl color-default dashboard">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<section>
    <!-- BEGIN SIDEBAR -->

@include('back.parts.sidebar')
<!-- END SIDEBAR -->
    <div class="main-content">
        <!-- BEGIN TOPBAR -->
        <div class="topbar">
            <div class="header-left">
                <div class="topnav">
                    <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span
                                class="menu__handle"><span>Menu</span></span></a>
                    {{--                    <ul class="nav nav-icons">--}}
                    {{--                        <li><a href="#" class="toggle-sidebar-top"><span class="icon-user-following"></span></a></li>--}}
                    {{--                        <li><a href="#"><span class="octicon octicon-mail-read"></span></a></li>--}}
                    {{--                        <li><a href="#"><span class="octicon octicon-flame"></span></a></li>--}}
                    {{--                        <li><a href="#"><span class="octicon octicon-rocket"></span></a></li>--}}
                    {{--                    </ul>--}}
                </div>
            </div>
            <div class="header-right">
                <ul class="header-menu nav navbar-nav">
                    <!-- BEGIN USER DROPDOWN -->
                {{--                    <li class="dropdown" id="language-header">--}}
                {{--                        <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">--}}
                {{--                            <i class="icon-globe"></i>--}}
                {{--                            <span>Language</span>--}}
                {{--                        </a>--}}
                {{--                        <ul class="dropdown-menu">--}}
                {{--                            <li>--}}
                {{--                                <a href="#" data-lang="en"><img src="{{asset('back/assets/global/images/flags/usa.png')}}"--}}
                {{--                                                                alt="flag-english"> <span>English</span></a>--}}
                {{--                            </li>--}}
                {{--                            <li>--}}
                {{--                                <a href="#" data-lang="es"><img src="{{asset('back/assets/global/images/flags/spanish.png')}}"--}}
                {{--                                                                alt="flag-english"> <span>Espaأ±ol</span></a>--}}
                {{--                            </li>--}}
                {{--                            <li>--}}
                {{--                                <a href="#" data-lang="fr"><img src="{{asset('back/assets/global/images/flags/french.png')}}"--}}
                {{--                                                                alt="flag-english"> <span>Franأ§ais</span></a>--}}
                {{--                            </li>--}}
                {{--                        </ul>--}}
                {{--                    </li>--}}
                <!-- END USER DROPDOWN -->

                    <li class="dropdown" id="user-header">
                        <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img src="{{asset((is_null(Auth::user()->image))?'/avatar/user1.png':'/avatar/'.Auth::user()->image->url)}}" alt="user image">
                            <span class="username">Hi,{{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{route('get-update-profile')}}"><i
                                            class="icon-user"></i><span>My Profile</span></a>
                            </li>
                            <li>
                                <form method="post" action="{{route('logout')}}" style="display: block">
                                    {{ csrf_field() }}
                                    <a href="#" onclick='this.parentNode.submit(); return false;'><i
                                                class="icon-logout"></i><span>Logout</span></a>
                                </form>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!-- header-right -->
        </div>
        <!-- END TOPBAR -->
        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content page-thin">
            <div class="row">
                <div class="col-md-12 col-financial-stocks">
                        @include('back.parts.quickview')
{{--                    @include('back.parts.search')--}}

                    @if(Session::has('flash_message'))
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="alert alert-success  alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <em> {!! session('flash_message') !!}</em>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-8 coMy Profilel-md-offset-2">
{{--                            @include ('errors.list')  Including error file --}}
                        </div>
                    </div>

                    @yield('content')

                </div>


            </div>
            <div class="footer">
                <div class="copyright">
                    <p class="pull-left sm-pull-reset">
                        <span>Copyright <span class="copyright">©</span> {{date('Y')}} </span>
                        <span>{{env('APP_NAME')}}</span>.
                        <span>All rights reserved. </span>
                    </p>

                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>

    </div>
    <!-- END BUILDER -->
</section>

<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>

    <script src="{{asset('back/assets/global/plugins/jquery/jquery-3.1.0.min.js')}}"></script>
    <script src="{{asset('back/assets/global/plugins/jquery/jquery-migrate-3.0.0.min.js')}}"></script>
    <script src="{{asset('back/assets/global/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

    <script src="{{asset('back/assets/global/plugins/tether/js/tether.min.js')}}"></script>

    <script src="{{asset('back/assets/global/plugins/appear/jquery.appear.js')}}"></script>
    <script src="{{asset('back/assets/global/plugins/gsap/main-gsap.min.js')}}"></script>
    <script src="{{asset('back/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('back/assets/global/plugins/jquery-cookies/jquery.cookies.min.js')}}"></script>
    <!-- Jquery Cookies, for theme -->
    <script src="{{asset('back/assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js')}}"></script>
    <!-- simulate synchronous behavior when using AJAX -->
    <script src="{{asset('back/assets/global/plugins/bootbox/bootbox.min.js')}}"></script>
    <!-- Modal with Validation -->
    <script src="{{asset('back/assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <!-- Custom Scrollbar sidebar -->
    <script src="{{asset('back/assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}"></script>
    <!-- Show Dropdown on Mouseover -->
    <script src="{{asset('back/assets/global/plugins/charts-sparkline/sparkline.min.js')}}"></script>
    <!-- Charts Sparkline -->
{{--    <script src="{{asset('back/assets/global/plugins/retina/retina.min.js')}}"></script> <!-- Retina Display -->--}}
{{--    <script src="{{asset('back/assets/global/plugins/select2/select2.min.js')}}"></script> <!-- Select Inputs -->--}}
{{--    <script src="{{asset('back/assets/global/plugins/icheck/icheck.min.js')}}"></script>--}}
    <!-- Checkbox & Radio Inputs -->
    <script src="{{asset('back/assets/global/plugins/backstretch/backstretch.min.js')}}"></script>
    <!-- Background Image -->
    <script src="{{asset('back/assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- Animated Progress Bar -->
{{--    <script src="{{asset('back/assets/global/js/builder.js')}}"></script> <!-- Theme Builder -->--}}
    <script src="{{asset('back/assets/global/js/sidebar_hover.js')}}"></script> <!-- Sidebar on Hover -->
    <script src="{{asset('back/assets/global/js/application.js')}}"></script> <!-- Main Application Script -->
    <script src="{{asset('back/assets/global/js/plugins.js')}}"></script> <!-- Main Plugin Initialization Script -->
    <script src="{{asset('back/assets/global/js/widgets/notes.js')}}"></script> <!-- Notes Widget -->
{{--    <script src="{{asset('back/assets/global/js/quickview.js')}}"></script> <!-- Chat Script -->--}}
{{--    <script src="{{asset('back/assets/global/js/pages/search.js')}}"></script> <!-- Search Script -->--}}
    <!-- BEGIN PAGE SCRIPT -->
    <script src="{{asset('back/assets/global/plugins/noty/jquery.noty.packaged.min.js')}}"></script>
    <!-- Notifications -->
    <script src="{{asset('back/assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js')}}"></script>
    <!-- Inline Edition X-editable -->
    <script src="{{asset('back/assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js')}}"></script>
    <!-- Context Menu -->
    <script src="{{asset('back/assets/global/plugins/multidatepicker/multidatespicker.min.js')}}"></script>
    <!-- Multi dates Picker -->
    <script src="{{asset('back/assets/global/js/widgets/todo_list.js')}}"></script>
{{--    <script src="{{asset('back/assets/global/plugins/metrojs/metrojs.min.js')}}"></script> <!-- Flipping Panel -->--}}
    {{--<script src="{{asset('back/assets/global/plugins/charts-chartjs/Chart.min.js')}}"></script>  <!-- ChartJS Chart -->--}}
    {{--<script src="{{asset('back/assets/global/plugins/charts-highstock/js/highstock.min.js')}}"></script>--}}
    <!-- financial Charts -->
    {{--<script src="{{asset('back/assets/global/plugins/charts-highstock/js/modules/exporting.min.js')}}"></script>--}}
    <!-- Financial Charts Export Tool -->
    {{--<script src="{{asset('back/assets/global/plugins/maps-amcharts/ammap/ammap.min.js')}}"></script> <!-- Vector Map -->--}}
    {{--<script src="{{asset('back/assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.min.js')}}"></script>--}}
    <!-- Vector World Map  -->
    {{--<script src="{{asset('back/assets/global/plugins/maps-amcharts/ammap/themes/black.min.js')}}"></script>--}}
    <!-- Vector Map Black Theme -->
{{--    <script src="{{asset('back/assets/global/plugins/skycons/skycons.min.js')}}"></script>--}}
    <!-- Animated Weather Icons -->
{{--    <script src="{{asset('back/assets/global/plugins/simple-weather/jquery.simpleWeather.js')}}"></script>--}}
    <!-- Weather Plugin -->
{{--    <script src="{{asset('back/assets/global/js/widgets/widget_weather.js')}}"></script>--}}
{{--    <script src="{{asset('back/assets/global/js/pages/dashboard.js')}}"></script>--}}
    <!-- END PAGE SCRIPT -->
    <script src="{{asset('back/assets/admin/layout1/js/layout.js')}}"></script>

<script>
    $(".action").on("submit", function () {
        return confirm("Do you sure for this action?");
    });
</script>
@yield('js')
</body>
</html>
