

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>SalesPage</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description"/>
    <meta content="themes-lab" name="author"/>
    <link rel="shortcut icon" href="{{asset('back/assets/global/images/favicon.png')}}">
    <link href="{{asset('back/assets/global/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('back/assets/global/css/ui.css')}}" rel="stylesheet">
    <link href="{{asset('back/assets/global/plugins/bootstrap-loading/lada.min.css')}}" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body class="account separate-inputs" data-page="login">
<!-- BEGIN LOGIN BOX -->
<div class="container" id="login-block">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
                <i class="user-img icons-faces-users-03"></i>
                <form class="form-signin" method="post" action="{{route('login')}}" role="form">
                    <div class="append-icon">
                        {{ csrf_field() }}
                        <input type="text" name="email" id="Email" class="form-control form-white Email"
                               placeholder="Email" required>
                        <i class="icon-user"></i>
                    </div>
                    @if ($error = $errors->first('email'))
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endif
                    <div class="append-icon m-b-20">
                        <input type="password" name="password" class="form-control form-white password"
                               placeholder="Password" required>
                        <i class="icon-lock"></i>
                    </div>
                    @if ($error = $errors->first('password'))
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endif

                    <button type="submit" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left">
                        Sign In
                    </button>

                </form>

            </div>
        </div>
    </div>

</div>
<script src="{{asset('back/assets/global/plugins/jquery/jquery-3.1.0.min.js')}}"></script>
<script src="{{asset('back/assets/global/plugins/jquery/jquery-migrate-3.0.0.min.js')}}"></script>
<script src="{{asset('back/assets/global/plugins/gsap/main-gsap.min.js')}}"></script>
<script src="{{asset('back/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('back/assets/global/plugins/backstretch/backstretch.min.js')}}"></script>
<script src="{{asset('back/assets/global/plugins/bootstrap-loading/lada.min.js')}}"></script>
<script src="{{asset('back/assets/global/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('back/assets/global/plugins/jquery-validation/additional-methods.min.js')}}"></script>
<script src="{{asset('back/assets/global/js/pages/login-v1.js')}}"></script>
</body>
</html>
