@extends('back.layout.master')

@section('title', '| Settings')

@section('content')
    <div class="row">

        <div class="col-lg-10 col-lg-offset-1">
            <h1><i class="fa fa-cog"></i> Edit Setting</h1>
            <hr>
            {!!Form::open(['route'=>['setting.update'],'files'=>true,'method'=>'post']) !!}

            <div class="row">
                <div class='col-lg-6'>
                    <div class="form-group">
                        {{ Form::label('logo', 'Logo') }}
                        {{ Form::file('logo', array('class' => 'form-control')) }}
                        @if ($errors->has('logo'))
                            <span class="text-danger">* {{ $errors->first('logo') }}</span>
                        @endif
                        <h6>*The logo must be a file of type: png.</h6>
                    </div>
                </div>
                <div class='col-lg-6'>
                    <div class="form-group">
                        <img style="width: 150px;" src="{{asset('back/assets/global/images/logo/logo-white.png')}}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='col-lg-12'>
                    <div class="form-group">
                        {{ Form::label('link', 'Link') }}
                        {{ Form::text('link', $setting['link']??"", array('class' => 'form-control')) }}
                        @if ($errors->has('link'))
                            <span class="text-danger">* {{ $errors->first('link') }}</span>
                        @endif
                    </div>
                </div>


            </div>

            <hr>
            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            {!! Form::close() !!}
        </div>
    </div>

@endsection
