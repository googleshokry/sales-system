@extends('back.layout.master')
@section('content')
    <div class="col-xlg-12 col-small-stats">
        <div class="row">
            <div class="col-xlg-4 col-lg-4 col-sm-4">
                <div class="panel">
                    <div class="panel-content widget-small bg-green">
                        <div class="title">
                            <h1><i
                                        class="fa fa-user"></i> <span>Users</span> </h1>
                            <p>{{$users}}</p>
                        </div>
                        <div class="content">
                            <div id="stock-virgin-sm"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xlg-4 col-lg-4 col-sm-4">
                <div class="panel">
                    <div class="panel-content widget-small bg-purple">
                        <div class="title">
                            <h1><i
                                        class="fa fa-th-list"></i><span> sales</span></h1>
                            <p>{{$sales}}</p>
                        </div>
                        <div class="content">
                            <div id="stock-ebay-sm"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @endsection
