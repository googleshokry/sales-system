@extends('back.layout.master')

@section('title', '| Edit Role')

@section('content')
    <div class="row">

    <div class='col-lg-4 col-lg-offset-4'>
        <h1><i class='fa fa-key'></i> Edit Role: {{$role->name}}</h1>
        <hr>

        {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            {{ Form::label('name', 'Role Name') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
            <h6>The Name may not be greater than 30 characters.</h6>
        @if ($errors->has('name'))
                <span class="text-danger">* {{ $errors->first('name') }}</span>
            @endif
        </div>

        <h5><b>Assign Permissions</b></h5>
        <div class="form-group {{ $errors->has('permissions') ? ' has-error' : '' }}">

        @foreach ($permissions as $permission)

            {{Form::checkbox('permissions[]',  $permission->id, $role->permissions ) }}
            {{Form::label($permission->name, ucfirst($permission->name)) }}<br>

        @endforeach
            @if ($errors->has('permissions'))
                <span class="text-danger">* {{ $errors->first('permissions') }}</span>
            @endif
        </div>
        <br>
        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}
    </div>
    </div>
@endsection