<div class='col-lg-2' style="margin-left: 10px;">
    <div class="form-group">
        @if($image->type=='img')
            <img style="width: 150px;display: inline-block" src="/products_view/{{$image->url}}">
            <a class="boxclose" href="{{route('product.delete.image',[$image->id,$product->id])}}"></a>
        @elseif($image->type=='video')
            <video width="150" controls>
                <source src="/files/{{$image->url}}" type="video/mp4">
                <source src="/files/{{$image->url}}" type="video/ogg">
                Your browser does not support the video tag.
            </video>
            <a class="boxclose" href="{{route('product.delete.image',[$image->id,$product->id])}}"></a>
        @endif
    </div>
</div>