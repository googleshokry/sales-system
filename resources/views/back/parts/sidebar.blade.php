<div class="sidebar">
    <div class="logopanel">
        <h1>
            <a href="/"></a>
        </h1>
    </div>
    <div class="sidebar-inner">
        <div class="sidebar-top">
            {{--            <form action="#" method="post" class="searchform" id="search">--}}
            {{--                <input type="text" class="form-control" name="keyword" placeholder="Search...">--}}
            {{--            </form>--}}
            <div class="userlogged clearfix">
                <img style="width:70px;float: left"
                     src="{{asset((is_null(Auth::user()->image))?'/avatar/user1.png':'/avatar/'.Auth::user()->image->url)}}"
                     alt="user image">
                <div class="user-details">
                    <h4>{{Auth::user()->name}}</h4>
                    <div class="dropdown user-login">
                        <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown"
                                data-hover="dropdown" data-close-others="true" data-delay="300">
                            <i class="online"></i><span>Available</span><i class="fa fa-angle-down"></i>
                        </button>
                        {{--                        <ul class="dropdown-menu">--}}
                        {{--                            <li><a href="#"><i class="busy"></i><span>Busy</span></a></li>--}}
                        {{--                            <li><a href="#"><i class="turquoise"></i><span>Invisible</span></a></li>--}}
                        {{--                            <li><a href="#"><i class="away"></i><span>Away</span></a></li>--}}
                        {{--                        </ul>--}}
                    </div>
                </div>

            </div>
        </div>
        <div class="menu-title">
            <div class="pull-right menu-settings">
                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300">--}}
                {{--<i class="icon-settings"></i>--}}
                {{--</a>--}}
                <ul class="dropdown-menu">
                    <li><a href="#" id="reorder-menu" class="reorder-menu">Reorder menu</a></li>
                    <li><a href="#" id="remove-menu" class="remove-menu">Remove elements</a></li>
                    <li><a href="#" id="hide-top-sidebar" class="hide-top-sidebar">Hide user &amp; search</a></li>
                </ul>
            </div>
        </div>
        <ul class="nav nav-sidebar">
            <li class="nav-active"><a href="{{route('dashboard')}}"><i class="icon-home"></i><span>Dashboard</span></a>
            </li>
            @can('users')
                <li class="nav-active"><a href="{{route('users.index')}}"><i
                            class="fa fa-user"></i><span>Users</span></a>
                </li>
                {{--<li class="nav-active"><a href="{{route('payment-link')}}"><i class="icon-home"></i><span>Send Link Payment</span></a></li>--}}
            @endcan
            @can('roles')
                <li class="nav-active"><a href="{{route('roles.index')}}"><i
                            class="fa fa-lock"></i><span>Roles</span></a>
                </li>
            @endcan

            @can('sales')
                <li class="nav-active"><a href="{{route('sales.index')}}"><i
                            class="fa fa-archive"></i><span>sales</span></a></li>
            @endcan
            @can('configrations')
            <li class="nav-active"><a href="{{route('configrations.index')}}"><i
                            class="fa fa-archive"></i><span>configrations</span></a></li>
            @endcan
            @can('settings')
                <li class="nav-active"><a href="{{route('setting.index')}}"><i
                            class="fa fa-history"></i><span>Settings</span></a></li>
            @endcan

        </ul>

        <div class="sidebar-footer clearfix">

            <a class="pull-left toggle_fullscreen" data-rel="tooltip" data-placement="top"
               data-original-title="Fullscreen">
                <i class="icon-size-fullscreen"></i></a>

            <form method="post" action="{{route('logout')}}" style="display: block">
                {{ csrf_field() }}
                <a class="pull-left btn-effect" href="#" data-modal="modal-1" data-rel="tooltip" data-placement="top"
                   data-original-title="Logout" onclick='this.parentNode.submit(); return false;'>
                    <i class="icon-power"></i></a>
            </form>
        </div>
    </div>
</div>
