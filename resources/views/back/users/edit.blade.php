@extends('back.layout.master')

@section('title', '| Edit User')

@section('content')
    <div class="row">

    <div class='col-lg-4 col-lg-offset-4'>

        <h1><i class='fa fa-user-plus'></i> Edit {{$user->name}}</h1>
        <hr>

        {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

        <div class="form-group  {{ $errors->has('name') ? ' has-error' : '' }}">
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
            <h6>The Name may not be greater than 30 characters.</h6>
        @if ($errors->has('name'))
                <span class="text-danger">* {{ $errors->first('name') }}</span>
            @endif
        </div>

        <div class="form-group  {{ $errors->has('email') ? ' has-error' : '' }}">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', null, array('class' => 'form-control')) }}
            @if ($errors->has('email'))
                <span class="text-danger">* {{ $errors->first('email') }}</span>
            @endif
        </div>

        <div class="form-group  {{ $errors->has('website') ? ' has-error' : '' }}">
            {{ Form::label('website', 'Website') }}
            {{ Form::text('website', null, array('class' => 'form-control')) }}
            @if ($errors->has('website'))
                <span class="text-danger">* {{ $errors->first('website') }}</span>
            @endif
        </div>

        <div class="form-group  {{ $errors->has('roles') ? ' has-error' : '' }}">
            {{ Form::label("roles","Roles")}}<br>
        @foreach ($roles as $role)
                {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
                {{ Form::label($role->name, ucfirst($role->name)) }}<br>

            @endforeach
                @if ($errors->has('roles'))
                    <span class="text-danger">* {{ $errors->first('roles') }}</span>
                @endif
        </div>

        <div class="form-group  {{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::label('password', 'Password') }}<br>
            {{ Form::password('password', array('class' => 'form-control')) }}
            <h6>The password must be at least 6 characters.</h6>
        @if ($errors->has('password'))
                <span class="text-danger">* {{ $errors->first('password') }}</span>
            @endif
        </div>

        <div class="form-group  {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            {{ Form::label('password', 'Confirm Password') }}<br>
            {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
            @if ($errors->has('password_confirmation'))
                <span class="text-danger">* {{ $errors->first('password_confirmation') }}</span>
            @endif
        </div>

        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

    </div>
    </div>
@endsection