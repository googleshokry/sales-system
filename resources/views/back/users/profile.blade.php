@extends('back.layout.master')

@section('title', '| Edit Profile')

@section('content')
    <div class="row">
        <div class='col-lg-4 col-lg-offset-4'>

            <h1><b>Your Profile</b></h1>
            {{ Form::open(array('route' => 'post-update-profile','files' => true)) }}

            <div class="form-group hidden">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_method" value="PATCH">
            </div>
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="email" class="control-label"><b>Name:</b></label>
                <input type="text" name="name" placeholder="Please enter your email here" class="form-control"
                       value="{{ $user->name }}"/>

                <?php if ($errors->has('name')) :?>
                <span class="help-block">
            <strong>{{$errors->first('name')}}</strong>
        </span>
                <?php endif;?>

            </div>
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label"><b>Email:</b></label>
                <input type="text" name="email" placeholder="Please enter your email here" class="form-control"
                       value="{{ $user->email }}"/>

                <?php if ($errors->has('email')) :?>
                <span class="help-block">
            <strong>{{$errors->first('email')}}</strong>
        </span>
                <?php endif;?>

                <div class="form-group {{ $errors->has('website') ? ' has-error' : '' }}">
                    <label for="website" class="control-label"><b>Website:</b></label>
                    <input type="text" name="website" placeholder="Please enter your website here" class="form-control"
                           value="{{ $user->website }}"/>

                    <?php if ($errors->has('website')) :?>
                    <span class="help-block">
            <strong>{{$errors->first('website')}}</strong>
        </span>
                    <?php endif;?>

            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="email" class="control-label"><b>Password:</b></label>
                <input type="password" name="password" placeholder="Please enter your Password here" class="form-control"
                />

                <?php if ($errors->has('password')) :?>
                <span class="help-block">
            <strong>{{$errors->first('password')}}</strong>
        </span>
                <?php endif;?>

            </div>
            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="password_confirmation" class="control-label"><b>Password Confirmation:</b></label>
                <input type="password" name="password_confirmation"
                       placeholder="Please enter your password_confirmation here" class="form-control"
                       value="{{ $user->password_confirmation }}"/>

                <?php if ($errors->has('password_confirmation')) :?>
                <span class="help-block">
            <strong>{{$errors->first('password_confirmation')}}</strong>
        </span>
                <?php endif;?>

            </div>
            <div class="form-group">
                <label for="avatar" class="control-label"><b>avatar:</b></label>
                <input type="file" name="avatar" class="form-control"/>
                <h6></h6>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default"> Submit</button>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection