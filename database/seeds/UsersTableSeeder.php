<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::query()->truncate();
        User::insert([[
            'name' => 'Super Admin',
            'email' => 'super@admin.com',
            'password' => bcrypt('admin123'),
            'super_admin' => 1,
            'email_verified_at' => '2021-02-26 21:55:48',
        ]]);
        User::insert([[
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456'),
            'email_verified_at' => '2021-02-26 21:55:48',
        ]]);

    }
}
