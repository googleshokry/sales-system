<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use \Spatie\Permission\Models\Role as Model;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
      //  Model::query()->truncate();
        Model::insert([
            ['name' => 'admin','guard_name' => 'web']
            ]
        );
        DB::table('role_has_permissions')->truncate();
        // user admin
        DB::table('role_has_permissions')->insert([
                ['permission_id' => '1','role_id' => '1'],
                ['permission_id' => '2','role_id' => '1'],
                ['permission_id' => '3','role_id' => '1'],
                ['permission_id' => '4','role_id' => '1'],
                ['permission_id' => '5','role_id' => '1'],
            ]
        );



        DB::table('model_has_roles')->truncate();
        DB::table('model_has_roles')->insert([
                ['role_id' => '1','model_type' => 'App\User','model_id'=>'1'],
            ]
        );
        DB::table('model_has_roles')->insert([
                ['role_id' => '1','model_type' => 'App\User','model_id'=>'2'],
            ]
        );
    }
}
