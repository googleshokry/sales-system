<?php

use Illuminate\Database\Seeder;
class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('settings')->truncate();
        DB::table('settings')->insert([
                ['key' => 'Link', 'value' => 'Localhost'],
                ['key' => 'icon', 'value' => 'https://www.flaticon.com/svg/vstatic/svg/4245/4245720.svg?token=exp=1614343611~hmac=9e7f928908eb2887778b592059574b5d'],
            ]
        );
    }
}
