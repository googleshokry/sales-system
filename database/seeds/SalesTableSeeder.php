<?php

use App\Salesman;
use Illuminate\Database\Seeder;


class SalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Salesman::query()->truncate();
        Salesman::insert([[
            'name' => 'Lala',
            'payment' => '200',
        ]]);
        Salesman::insert([[
            'name' => 'Dipsy',
            'payment' => '100',
        ]]);

    }
}
