<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use \Spatie\Permission\Models\Permission as Model;
class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
       // Model::query()->truncate();
        Model::insert([
            ['name' => 'users','guard_name' => 'web'],
            ['name' => 'roles','guard_name' => 'web'],
            ['name' => 'sales','guard_name' => 'web'],
            ['name' => 'configrations','guard_name' => 'web'],
            ['name' => 'settings','guard_name' => 'web'],
            ]
        );
    }
}
