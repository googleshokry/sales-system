<?php

use App\Configration;
use Illuminate\Database\Seeder;

class ConfigrationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Configration::query()->truncate();
        Configration::insert([[
            'target' => '3500',
            'sale_id' => '1',
        ]]);
        Configration::insert([[
            'target' => '20000',
            'sale_id' => '2',
        ]]);

    }
}
